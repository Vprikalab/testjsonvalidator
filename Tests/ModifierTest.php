<?php



namespace Tests;

require_once 'vendor/autoload.php';

use Domain\Sanitaizer\Sanitaizer;

class ModifierTest extends \PHPUnit\Framework\TestCase
{
    private function createSanitaizer(array $data)
    {
        return new Sanitaizer(json_encode($data));
    }

    public function test_modify_phone_number()
    {
        $sanitaizer = $this->createSanitaizer(
            [
                'phone' => '+7 (800) 600-30-30'
            ]
        );

        $sanitaizer->setRules(
            [
                'phone' => 'phone'
            ]
        );

        $sanitaizer->validate();

        $this->assertEquals('78006003030', $sanitaizer->phone);
    }
}
