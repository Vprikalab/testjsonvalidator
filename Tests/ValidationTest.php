<?php

require_once 'vendor/autoload.php';

use Domain\Sanitaizer\Sanitaizer;

class ValidationTest extends \PHPUnit\Framework\TestCase
{

    private function createSanitaizer(array $data)
    {
        return new Sanitaizer(json_encode($data));
    }

    public function test_string_validation()
    {
        $sanitaizer = $this->createSanitaizer(['abc' => '123']);

        $sanitaizer->setRules(['abc' => 'string']);

        $sanitaizer->validate();

        $this->assertEquals([], $sanitaizer->getErrorBag());
    }

    public function test_numeric_validate()
    {
        $sanitaizer = $this->createSanitaizer(['abc' => '123.123']);

        $sanitaizer->setRules(['abc' => 'numeric']);

        $sanitaizer->validate();

        $this->assertEquals([], $sanitaizer->getErrorBag());
    }

    public function test_array_structure_validate()
    {
        $sanitaizer = $this->createSanitaizer(
            [
                'abc' =>
                    [
                        'name' => '123'
                    ]
            ]
        );

        $sanitaizer->setRules([
            'abc.name' => 'string'
        ]);

        $sanitaizer->validate();

        $this->assertEquals([], $sanitaizer->getErrorBag());
    }

    public function test_required_value()
    {
        $sanitaizer = $this->createSanitaizer(
            [
                'abc' =>
                    [
                        'name' => '123'
                    ]
            ]
        );

        $sanitaizer->setRules([
            'abc.name.surname' => 'required'
        ]);

        $sanitaizer->validate();

        $this->assertEquals([ 'abc.name.surname Must be of type required' ], $sanitaizer->getErrorBag());
    }

    public function test_many_rules_for_field()
    {
        $sanitaizer = $this->createSanitaizer(
            [
                'abc' =>
                    [
                        'name' => [
                            'surname' => 'abc'
                        ]
                    ]
            ]
        );

        $sanitaizer->setRules([
            'abc.name.surname' => 'required | string'
        ]);

        $sanitaizer->validate();

        $this->assertEquals([], $sanitaizer->getErrorBag());
    }

    public function test_many_fields_validation()
    {
        $sanitaizer = $this->createSanitaizer(
            [
                'abc' =>
                    [
                        'name' => [
                            'surname' => 'abc',
                            'children' => [
                                [
                                    'name' => 'Mike'
                                ]
                            ]
                        ]
                    ]
            ]
        );

        $sanitaizer->setRules([
            'abc.name.surname' => 'required | string',
            'abc.name.children.*.name' => 'required | string'
        ]);

        $sanitaizer->validate();

        $this->assertEquals([], $sanitaizer->getErrorBag());
    }

    public function test_non_required_field()
    {
        $sanitaizer = $this->createSanitaizer(
            [
                'abc' =>
                    [
                        'name' => [
                            'surname' => 'abc'
                        ]
                    ]
            ]
        );

        $sanitaizer->setRules([
            'abc.name.child.name' => 'string'
        ]);

        $sanitaizer->validate();

        $this->assertEquals([], $sanitaizer->getErrorBag());
    }

    /**
     * @dataProvider phoneDataSet
     * @param string $phone
     * @param array $expectErrorBag
     */
    public function test_phone_number_validation(string $phone, array $expectErrorBag)
    {
        $sanitaizer = $this->createSanitaizer(
            [
                'phone' => $phone
            ]
        );

        $sanitaizer->setRules([
            'phone' => 'phone'
        ]);

        $sanitaizer->validate();

        $this->assertEquals($expectErrorBag, $sanitaizer->getErrorBag());
    }

    public function phoneDataSet()
    {
        return [
            [
                '88005553535',
                []
            ],
            [
                'asd',
                [
                    'phone Must be of type phone'
                ]
            ]
        ];
    }

    public function test_error_bag()
    {
        $sanitaizer = $this->createSanitaizer(
            [
                'Name' => 123
            ]
        );

        $sanitaizer->setRules([
            'Name' => 'string'
        ]);

        $sanitaizer->validate();

        $this->assertEquals(
            [
                'Name Must be of type string'
            ],
            $sanitaizer->getErrorBag()
        );
    }

    public function test_validation_rule_not_found()
    {
        $sanitaizer = $this->createSanitaizer(
            [
                'name' => 'Mike'
            ]
        );

        $sanitaizer->setRules([
            'name' => 'name'
        ]);

        $this->expectException(\Domain\Validator\Exceptions\RuleNotFoundException::class);

        $sanitaizer->validate();
    }
}
