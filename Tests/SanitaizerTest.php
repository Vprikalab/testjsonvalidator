<?php

require_once 'vendor/autoload.php';

use Domain\Sanitaizer\Sanitaizer;

class SanitaizerTest extends \PHPUnit\Framework\TestCase
{
    private function createSanitaizer(array $data)
    {
        return new Sanitaizer(json_encode($data));
    }

    public function test_get_magic_property()
    {
        $sanitaizer = $this->createSanitaizer(
            [
                'person' => [
                    'name' => 'Mike'
                ]
            ]
        );

        $this->assertEquals('Mike', $sanitaizer->person['name']);
    }

    public function test_implementation()
    {
        $data = json_encode([
            'name' => 'Mike',
            'surname' => 'Chain',
            'phone' => '+7 800 555-35-35',
            'balance' => 13000.34
        ]);

        $sanitaizer = new \example\StoreRequestSanitaizer($data);

        $this->assertEquals([], $sanitaizer->getErrorBag());

        $this->assertEquals('Mike', $sanitaizer->name);

        $this->assertEquals('Chain', $sanitaizer->surname);

        $this->assertEquals('78005553535', $sanitaizer->phone);

        $this->assertEquals(13000.34, $sanitaizer->balance);

        $this->assertEquals(null, $sanitaizer->city);
    }
}
