<?php

namespace example;
use Domain\Sanitaizer\Sanitaizer;

class StoreRequestSanitaizer extends Sanitaizer
{
    public function rules(): array
    {
        return [
            'name' => 'string | required',
            'surname' => 'string | required',
            'phone' => 'phone | required',
            'balance' => 'float | required',
            'city' => 'string'
        ];
    }
}
