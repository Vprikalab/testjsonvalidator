<?php


namespace Domain\Validator\Exceptions;


use Throwable;

class RuleNotFoundException extends \Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct('Rule ' . $message . ' not found', $code, $previous);
    }
}
