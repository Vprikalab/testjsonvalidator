<?php


namespace Domain\Validator;


use Domain\Validator\Abstracts\ValidatorInterface;
use Domain\Validator\Exceptions\RuleNotFoundException;

class Validator implements ValidatorInterface
{
    /** @inheritDoc */
    public static function validate($value, string $rule): bool
    {
        if (!method_exists(self::class, $rule)) {
            throw new RuleNotFoundException($rule);
        }
        return self::$rule($value);
    }

    /** @inheritDoc */
    public static function string($string): bool
    {
        return is_string($string) || is_null($string);
    }

    /** @inheritDoc */
    public static function numeric($numeric):bool
    {
        return is_numeric($numeric) || is_null($numeric);
    }

    /** @inheritDoc */
    public static function required($value): bool
    {
        return isset($value);
    }

    /** @inheritDoc */
    public static function phone($phone): bool
    {
        return preg_match('/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/', $phone) || is_null($phone);
    }

    /** @inheritDoc */
    public static function float($float): bool
    {
        return is_float($float) || is_null($float);
    }
}
