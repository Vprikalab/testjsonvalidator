<?php

namespace Domain\Validator\Abstracts;

use Domain\Validator\Exceptions\RuleNotFoundException;

interface ValidatorInterface
{
    /**
     * Validate a value by rule
     * @param $value
     * @param string $rule
     * @return bool
     * @throws RuleNotFoundException
     */
    static function validate($value, string $rule): bool;

    /**
     * Validate string
     * @param $string
     * @return bool
     */
    static function string($string): bool;

    /**
     * Validate numeric
     * @param $numeric
     * @return bool
     */
    static function numeric($numeric): bool;

    /**
     * Validate required
     * @param $value
     * @return bool
     */
    static function required($value): bool;

    /**
     * Validate phone
     * @param $phone
     * @return bool
     */
    static function phone($phone): bool ;

    /**
     * Validate float
     * @param $float
     * @return bool
     */
    static function float($float): bool;
}
