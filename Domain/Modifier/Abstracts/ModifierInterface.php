<?php


namespace Domain\Modifier\Abstracts;


interface ModifierInterface
{
    /**
     * Modify parameter by rule
     * @param $value
     * @param $rule
     * @return mixed
     */
    static function modify($value, string $rule);

    /**
     * Modify phone number
     * @param string $phone
     * @return string
     */
    static function phone(string $phone): string;
}
