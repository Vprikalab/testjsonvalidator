<?php


namespace Domain\Modifier;


use Domain\Modifier\Abstracts\ModifierInterface;

class Modifier implements ModifierInterface
{
    /** @inheritDoc */
    public static function modify($value, string $rule)
    {
        return self::$rule($value);
    }

    /** @inheritDoc */
    public static function phone(string $phone): string
    {
        $phone = str_replace(' ', '', $phone);

        $phone = str_replace('(', '', $phone);

        $phone = str_replace(')', '', $phone);

        $phone = str_replace('-', '', $phone);

        $phone = str_replace('+', '', $phone);

        if ($phone[0] === '8') {
            $phone[0] = '7';
        }

        return $phone;
    }
}
