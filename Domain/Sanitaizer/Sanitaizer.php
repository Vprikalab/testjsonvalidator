<?php

namespace Domain\Sanitaizer;

use Domain\Modifier\Abstracts\ModifierInterface;
use Domain\Modifier\Modifier;
use Domain\Sanitaizer\Exceptions\ValidationFailsException;
use Domain\Sanitaizer\Abstracts\SanitaizerInterface;
use Domain\Validator\Abstracts\ValidatorInterface;
use Domain\Validator\Validator;

class Sanitaizer implements SanitaizerInterface
{
    /** @var ValidatorInterface  */
    private $defaultValidator = Validator::class;

    /** @var ModifierInterface  */
    private $defaultModifier = Modifier::class;

    /** @var ValidatorInterface */
    private $validator;

    /** @var ModifierInterface */
    private $modifier;

    /** @var array */
    private $data;

    /** @var array */
    private $rules;

    /** @var array */
    private $errorBag = [];

    /**
     * Sanitaizer constructor.
     * @param string $jsonData
     * @param ValidatorInterface $validator
     * @param ModifierInterface|null $modifier
     */
    public function __construct(string $jsonData, ValidatorInterface $validator = null, ModifierInterface $modifier = null)
    {
        $this->setRules($this->rules());

        $this->data = json_decode($jsonData, true);

        if (isset($validator)) {
            $this->setValidator($validator);
        } else {
            $this->setValidator(new $this->defaultValidator());
        }

        if ($modifier) {
            $this->setModifier($modifier);
        } else {
            $this->setModifier(new $this->defaultModifier());
        }

        $this->validate();

        return $this->errorBag;
    }

    /**
     * @param $name
     * @return mixed|null
     */
    public function __get($name)
    {
        return isset($this->data[$name]) ? $this->data[$name] : null;
    }

    /** @inheritDoc */
    public function rules(): array
    {
        return [];
    }

    /** @inheritDoc */
    public function setRules(array $rules)
    {
        $this->rules = $rules;
    }

    /** @inheritDoc */
    public function getRules(): array
    {
        return $this->rules;
    }

    /** @inheritDoc */
    public function setValidator(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /** @inheritDoc */
    public function setModifier(ModifierInterface $modifier)
    {
        $this->modifier = $modifier;
    }

    /** @inheritDoc */
    public function validate()
    {
        $rules = $this->getRules();

        $structure = $this->makeStructure($rules);

        $this->structureRunner($structure, $this->data);

    }

    /**
     * Go by all variable in structure, validate and modify this
     * @param $structure
     * @param $data
     * @param string|null $field_name
     * @return bool
     */
    private function structureRunner($structure, &$data, string $field_name = null)
    {
        if (is_string($structure)) {
            foreach ($this->makeRules($structure) as $rule) {
                if (!$this->validator::validate($data, $rule)) {
                    if (is_string($field_name)) {
                        $field_name = substr($field_name, 0,strlen($field_name) - 1);
                    }
                    $this->makeValidationError($field_name, $rule);
                }
                if (method_exists($this->modifier, $rule)) {
                    $data = $this->modifier::$rule($data);
                }
            }
            return true;
        }

        foreach ($structure as $field => $value) {

            $field_path = $field_name . $field . '.';

            if ($field === '*') {
                foreach ($data as $item) {
                    $this->structureRunner($structure['*'], $item, $field_path);
                }
                continue;
            }

            if (!isset($data[$field])) {
                $fakeData = [];
                $data = &$fakeData;
                $data[$field] = null;
            }

            $this->structureRunner($value, $data[$field], $field_path);
        }
    }

    /**
     * Convert variable path from string to array
     * @param array $rules
     * @return array
     */
    private function makeStructure(array $rules): array
    {
        $structure = [];

        foreach ($rules as $field=>$rule) {

            $fieldStructure = [];

            $fieldStructureLink = &$fieldStructure;

            foreach (explode('.', $field) as $value) {
                 $fieldStructureLink = &$fieldStructureLink[$value];
            }

            $fieldStructureLink = $rule;

            $structure = array_merge_recursive($structure, $fieldStructure);
        }

        return $structure;
    }

    /**
     * Make validation rules from string to array
     * @param string $rules
     * @return array
     */
    private function makeRules(string $rules) : array
    {
        return explode('|',str_replace(' ', '', $rules));
    }

    /**
     * Pass a validation error to bag
     * @param string $field
     * @param string $expectedType
     */
    private function makeValidationError(string $field, string $expectedType)
    {
        $this->errorBag[] = $field . ' Must be of type ' . $expectedType;
    }

    /** @inheritDoc */
    public function getErrorBag()
    {
        return $this->errorBag;
    }
}
