<?php


namespace Domain\Sanitaizer\Abstracts;


use Domain\Modifier\Abstracts\ModifierInterface;
use Domain\Validator\Abstracts\ValidatorInterface;

interface SanitaizerInterface
{
    /**
     * Return rules for data validation
     * @return array
     */
    public function rules(): array;

    /**
     * Set validator
     * @param ValidatorInterface $validator
     */
    public function setValidator(ValidatorInterface $validator);

    /**
     * Set modifier
     * @param ModifierInterface $modifier
     * @return mixed
     */
    public function setModifier(ModifierInterface $modifier);

    /**
     * Set Rules
     * @param array $rules
     */
    public function setRules(array $rules);

    /**
     * Get Rules
     * @return array
     */
    public function getRules(): array;

    /**
     * Make validation
     * @return mixed
     */
    public function validate();
}
