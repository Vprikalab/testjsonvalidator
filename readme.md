**Test case of json validator**<br />
Sanitaizer - test project, it's validate json structure and types.<br />
To use sanitaizer u need to implement Sanitaizer class, overwrite rules method, create a new instance of your object and pass json string into a constructor.<br />
**Available validation rules**
- required
- string
- numeric (pass float and integer)
- float
- phone (example: +7 (800) 600-30-30)

**Note**: <br />
If you pass variable contains type (not required), and variable don't exist or equals `null`, validator will't make error. If you need check for variable exist, just add `required` rule.

You can combine rules by implode by `|` <br />
Example: 
```
public function rules()
{
    return [
        'name' => 'string | required',
        'phone' => 'phone',
        'balance' => 'float | required'
    ];
}
```
Also, you can modify your fields <br />
Available modify rules:
- phone

For changing or update validation and modify rules you just need implement ValidatorInterface and ModifierInterface and pass them into a constructor.<br />

After validation, you can see validation errors using `$object->getErrorBag()` <br />

To access data after validation and modifying, just call object property<br />
Example: <br />
`$object->phone`<br /> 
Using example:
```
class StoreRequestSanitaizer extends Sanitaizer
{
    public function rules(): array
    {
        return [
            'name' => 'string | required',
            'surname' => 'string | required',
            'phone' => 'phone | required',
            'balance' => 'integer | required',
            'city' => 'string'
        ];
    }
}
```
```
$sanitaizer = new StoreRequestSanitaizer({'name' : 'Mike', 'surname' : 'Chain' , 'phone' : '+7 (800) 600-30-30, 'balance' : 'abc})

$sanitaizer->getErrorBag()
```
> ['balance Must be of type float\']

```
$sanitaizer->name
```
> Mike

```
$sanitaizer->phone
```

> 7800603030

```
$sanitaizer->city
```
> null






